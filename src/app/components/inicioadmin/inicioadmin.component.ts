import { Component, OnInit , ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import { SitioTurisService } from '../../services/services.sitioturis/sitio.service';
import { UploadFileService } from '../../services/services.sitioturis/upload-file.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { Sturis } from 'src/app/interfaces/interfaces.sitioturis/sitioturis.interface';
import {Router} from '@angular/router';



@Component({
  selector: 'app-inicioadmin',
  templateUrl: './inicioadmin.component.html',
  styleUrls: ['./inicioadmin.component.css']
})
export class InicioadminComponent implements OnInit {
  @ViewChild('closeModalCreate') closeModalCreate;
  selectedFiles: FileList;
  selectedImg: FileList;
  nombreSitio = new FormControl('');
  ubicacionSitio = new FormControl('');
  nitSitio = new FormControl('');
  estadoSitio = new FormControl('');
  descripSitio = new FormControl('');




  info: Sturis[] = [];
  consulNombre: string;
  public descripcionEdit: string;
  public estadoEdit: string;
  public guiaEdit: string;
  public nitEdit: string;
  public nombreEdit: string;
  public ubicacionEdit: string;

  private guiaSitioEdit: string;
  private descripcionSitioEdit: string;


  constructor(private SitioService: SitioTurisService, private router: Router, private uploadService: UploadFileService) {
  }

  ngOnInit(): void {
    this.todoSitios();
    /* this.fileInfos = this.uploadService.getFiles();*/
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  selectImg(event) {
    this.selectedImg = event.target.files;
  }

  crearSitio() {
    let estado;
    if (this.estadoSitio.value === 'Destacado') {
      estado = 1;
    } else {
      estado = 0;
    }
    this.SitioService.guardarSitio(this.nombreSitio.value, this.ubicacionSitio.value, this.nitSitio.value, estado,
      this.descripSitio.value).subscribe(response => {
      this.uploadArchivo();
      this.uploadImagen();
    });
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: `El sitio turistico se ha registrado exitosamente`,
      showConfirmButton: false,
      timer: 2500
    });
    this.todoSitios();
    this.closeModalCreate.nativeElement.click(); // Cerrar modal
  }

  upload( file) {
    this.uploadService.upload(file, this.nombreSitio.value).subscribe();
  }

  uploadImagen() {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.selectedImg.length; i++) {
      this.upload(this.selectedImg[i]);
    }
  }

  uploadArchivo() {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(this.selectedFiles[i]);
    }
  }

  todoSitios() {
    this.SitioService.Todos().subscribe((info: Sturis[]) => {
      this.info = info;
    });
  }

  Search() {
    const nombreInput = ' ' + this.consulNombre;
    const nombreTuris = this.info.filter(nombre => nombre.sturis_nombre === nombreInput);
    if (Object.entries(nombreTuris).length === 0) {

      Swal.fire({
        position: 'center',
        icon: 'error',
        title: `El nombre escrito no se encuentra registrado`,
        showConfirmButton: false,
        timer: 2800
      });
    } else {
    this.info = nombreTuris;
    }
  }

  // tslint:disable-next-line:variable-name
  eliminarSitio(sturis_id: number) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: '¡No podrás revertir esto!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, bórralo!'
    }).then((result) => {
      this.SitioService.eliminarSitio(sturis_id).subscribe();
      if (result.value) {
        Swal.fire(
          '¡Eliminado!',
          'Sus datos han sido eliminados.',
          'success'
        );
        this.ngOnInit();
      }
    });
  }

  editarSitios() {
    let estado;
    if (this.estadoSitio.value === 'Destacado') {
      estado = 1;
    } else {
      estado = 0;
    }
    let nombreEditar;
    let ubicacionEditar;
    let descripcionEditar;
    let nitEditar;
    if (this.nombreSitio.value === '' ) {
      nombreEditar = this.nombreEdit;
    } else {
      nombreEditar = this.nombreSitio.value;
    }
    if (this.ubicacionSitio.value === '') {
      ubicacionEditar = this.ubicacionEdit;
    } else {
      ubicacionEditar = this.ubicacionSitio.value;
    }
    if (this.descripSitio.value === '') {
        descripcionEditar = this.descripcionEdit;
    } else {
      descripcionEditar = this.descripSitio.value;
    }
    if (this.nitSitio.value === '') {
      nitEditar = this.nitEdit;
    } else {
      nitEditar = this.nitSitio.value;
    }
    this.SitioService.editarSitio(nombreEditar, ubicacionEditar, descripcionEditar, estado, nitEditar).subscribe(response => {
      this.uploadArchivo();
      this.uploadImagen();
    });
  }

  // tslint:disable-next-line:variable-name
  consultarEditar(sturis_nombre: string) {
    const nombreInput = sturis_nombre;
    this.SitioService.buscar(nombreInput).subscribe((infoSitio: Sturis[]) => {
      // tslint:disable-next-line:forin
      for (const key in infoSitio) {
        this.descripcionEdit = infoSitio[key].sturis_descripcion;
        this.nitEdit = infoSitio[key].sturis_nit;
        this.nombreEdit = infoSitio[key].sturis_nombre;
        this.ubicacionEdit = infoSitio[key].sturis_ubicacion;

      }
    });

  }

}
