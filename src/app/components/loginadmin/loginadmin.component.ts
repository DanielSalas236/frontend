import { Component, OnInit} from '@angular/core';
import { LoginAdminService } from '../../services/services.admin/service.login/login.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-loginadmin',
  templateUrl: './loginadmin.component.html',
  styleUrls: ['./loginadmin.component.css']
})
export class LoginadminComponent implements OnInit {
  username = new FormControl('');
  password = new FormControl('');



  constructor(private LoginService: LoginAdminService, private router: Router, location: Location) {

  }


  ngOnInit(): void {

  }


  enviarLogin() {
    this.LoginService.LoginValidate(this.username.value, this.password.value).subscribe(response => {
      let usuario;
      let contrasenha;
      let rol;
      // tslint:disable-next-line:forin
      for (const key in response) {
        usuario = response[key].usu_username;
        contrasenha = response[key].usu_password;
        rol = response[key].rol_rol_id;

      }

      if (usuario === this.username.value && contrasenha === this.password.value && rol === 1) {

        this.router.navigate(['/inicioadmin']);

      } else if (usuario !== this.username.value && contrasenha !== this.password.value && rol !== 1) {

        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `usuario o contraseña inválidos`,
          showConfirmButton: false,
          timer: 1500
        });

      } else if (rol !== 1 || rol == null) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `El usuario que digito no cuenta con el rol de administrador`,
          showConfirmButton: false,
          timer: 2500
        });
      }
    });

  }
}
