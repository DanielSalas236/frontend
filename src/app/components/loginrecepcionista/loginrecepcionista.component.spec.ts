import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginrecepcionistaComponent } from './loginrecepcionista.component';

describe('LoginrecepcionistaComponent', () => {
  let component: LoginrecepcionistaComponent;
  let fixture: ComponentFixture<LoginrecepcionistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginrecepcionistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginrecepcionistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
