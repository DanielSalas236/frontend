import {  Component,  OnInit} from '@angular/core';
import {  LoginRecepService} from '../../services/services.recepcionista/service.login/login.service';
import Swal from 'sweetalert2';
import {  Router} from '@angular/router';
import {  Location} from '@angular/common';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-loginrecepcionista',
  templateUrl: './loginrecepcionista.component.html',
  styleUrls: ['./loginrecepcionista.component.css']
})
export class LoginrecepcionistaComponent implements OnInit {
  username = new FormControl('');
  password = new FormControl('');

  constructor(private LoginServiceRecep: LoginRecepService, private router: Router, location: Location) {}

  ngOnInit(): void {}

  enviarLogin() {
    this.LoginServiceRecep.Login(this.username.value, this.password.value).subscribe(response => {
      console.log(response);
      let usuario;
      let contrasenha;
      let rol;
      // tslint:disable-next-line:forin
      for (const key in response) {
        usuario = response[key].usu_username;
        contrasenha = response[key].usu_password;
        rol = response[key].rol_rol_id;
      }
      if (usuario === this.username.value && contrasenha === this.password.value && rol === 2) {

        this.router.navigate(['/iniciorecep']);

      } else if (usuario !== this.username.value && contrasenha !== this.password.value && rol !== 2) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `usuario o contraseña inválidos`,
          showConfirmButton: false,
          timer: 1500
        });

      } else if (rol !== 2 || rol == null) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `El usuario que digito no cuenta con el rol de recepcionista`,
          showConfirmButton: false,
          timer: 2500
        });
      }
    });

  }
}
