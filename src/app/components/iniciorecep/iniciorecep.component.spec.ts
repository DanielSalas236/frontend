import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciorecepComponent } from './iniciorecep.component';

describe('IniciorecepComponent', () => {
  let component: IniciorecepComponent;
  let fixture: ComponentFixture<IniciorecepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciorecepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciorecepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
