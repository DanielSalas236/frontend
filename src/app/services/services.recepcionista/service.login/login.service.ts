import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  UserRecep
} from '../../../interfaces/interface.recep/interface.login/login.interface';
import {
  Observable
} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginRecepService {

  private url = 'http://localhost:8090/sitiosturisticos-app';


  constructor(private http: HttpClient) {}

  Login(user: string, password: string): Observable < UserRecep > {
    return this.http.get < UserRecep > (this.url + '/login/iniciar?username=' + user + '&password=' + password);
  }
}
