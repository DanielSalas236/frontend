import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../../../interfaces/interfaces.admin/interface.login/login.interface';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginAdminService {
  private url = 'http://localhost:8090/sitiosturisticos-app';
  constructor(private http: HttpClient) {}

  LoginValidate(user: string, password: string): Observable < Users > {
    return this.http.get < Users > (this.url + '/login/iniciar?username=' + user + '&password=' + password);
  }

}
