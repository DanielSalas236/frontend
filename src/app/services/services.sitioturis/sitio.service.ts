import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class SitioTurisService {
  private url = 'http://localhost:8091/sitiosturisticos-app';
  constructor(private http: HttpClient) {}

  Todos() {
    return this.http.get(this.url + '/sitio/todos');
  }

  guardarSitio(nombreSitio: string, ubicacionSitio: string, nitSitio: number,  estadoSitio: number,  descripSitio: string) {
    // tslint:disable-next-line:max-line-length
     return this.http.get(this.url + '/sitio/guardar?descripcion=' + descripSitio + '&estado=' + estadoSitio + '&nit=' + nitSitio + '&nombre= ' + nombreSitio + '&ubicacion=' + ubicacionSitio);
  }

  eliminarSitio(id: number) {
    return this.http.delete(this.url + '/sitio/delete?id=' + id );
  }

  editarSitio(nombreSitio: string, ubicacionSitio: string, descripSitio: string, estadoSitio: number, nitSitio: string) {
    // tslint:disable-next-line:max-line-length
      return this.http.get(this.url + '/sitio/actualizar?descripcion=' + descripSitio + '&estado=' + estadoSitio + '&nit=' + nitSitio + '&nombre= ' + nombreSitio + '&ubicacion=' + ubicacionSitio);
  }
  buscar(nombre: string) {
    return this.http.get(this.url + '/sitio/buscar?nombre=' + nombre );
  }


}
