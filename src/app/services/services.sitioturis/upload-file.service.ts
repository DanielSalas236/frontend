import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import {compareSegments} from "@angular/compiler-cli/ngcc/src/sourcemaps/segment_marker";

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  private baseUrl = 'http://localhost:8091/sitiosturisticos-app';

  constructor(private http: HttpClient) {
  }

  private req;

  upload(file: File, nombre: string): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('nombre', nombre);
    this.req = new HttpRequest('POST', `${this.baseUrl}/sitio/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(this.req);
  }
}
