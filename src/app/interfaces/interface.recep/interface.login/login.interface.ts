export interface UserRecep {
  usu_id: number;
  usu_nombre: string;
  usu_apellido: string;
  usu_cedula: string;
  usu_telefono: number;
  usu_direccion: string;
  usu_email: string;
  usu_password: string;
  usu_username: string;
  rol_rol_id: number;
  tipo_documento_id: number;
}
