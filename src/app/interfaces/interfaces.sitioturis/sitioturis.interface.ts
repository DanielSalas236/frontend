export interface Sturis {
    sturis_id: number;
    sturis_nombre: string;
    sturis_ubicacion: string;
    sturis_nit: string;
    sturis_descripcion: string;
    sturis_estado: string;
    sturis_guia: string;
    mensaje: string;
}
