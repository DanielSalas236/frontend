import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LoginadminComponent } from './components/loginadmin/loginadmin.component';
import { LoginrecepcionistaComponent } from './components/loginrecepcionista/loginrecepcionista.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { InicioadminComponent } from './components/inicioadmin/inicioadmin.component';
import { IniciorecepComponent } from './components/iniciorecep/iniciorecep.component';



const routes: Routes = [

  {path: '', component: LoginComponent},
  {path: 'loginadmin', component: LoginadminComponent},
  {path: 'loginrecepcionista', component: LoginrecepcionistaComponent},
  {path: 'cliente', component: ClienteComponent},
  {path: 'inicioadmin', component: InicioadminComponent},
  {path: 'iniciorecep', component: IniciorecepComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
