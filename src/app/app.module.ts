import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './components/roles/admin/admin.component';
import { LoginComponent } from './components/login/login.component';
import { LoginadminComponent } from './components/loginadmin/loginadmin.component';
import { LoginrecepcionistaComponent } from './components/loginrecepcionista/loginrecepcionista.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginAdminService } from '../app/services/services.admin/service.login/login.service';
import { InicioadminComponent } from './components/inicioadmin/inicioadmin.component';
import { IniciorecepComponent } from './components/iniciorecep/iniciorecep.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    LoginadminComponent,
    LoginrecepcionistaComponent,
    InicioadminComponent,
    IniciorecepComponent,


  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule

    ],
  providers: [
    LoginAdminService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
